#include "rs_utils.h"
#include <stdio.h>
//uint8_t transmit_data[N];
//uint8_t parity[32];
uint8_t _data[256];

void printData(uint8_t *data, uint8_t len){
	for(int i = 0; i < len; i++)
		printf("%u ", data[i]);
	printf("\n");
}

void rs_encode(uint8_t *parity, uint8_t *data, size_t len){
	memset(&_data[0], 0, 256 * sizeof(uint8_t));
	memcpy(&_data[0], &data[0], len * sizeof(uint8_t));
	rs_utils_encode(&_data[223], &_data[0], 223);
	memcpy(&parity[0], &_data[223], 32 * sizeof(uint8_t));
}

int rs_decode(uint8_t *data, size_t len){
	memset(&_data[0], 0, 256 * sizeof(uint8_t));
	memcpy(&_data[0], &data[0], (len-32) * sizeof(uint8_t));
	memcpy(&_data[223], &data[len-32], 32 * sizeof(uint8_t));
	int result = rs_utils_decode(&_data[0], 223);
	memcpy(&data[0], &_data[0], (len-32) * sizeof(uint8_t));
	memcpy(&data[len-32], &_data[223], 32 * sizeof(uint8_t));
	return result;
}

int main(){
	
	int bytes;
	int N;
	int len = 25;
	uint8_t message[len];
	memset(message, 42, len*sizeof(uint8_t));
	uint8_t parity[32];
	rs_encode(&parity[0], &message[0], len);
	
	
	uint8_t tx_data[len + 32];
	memcpy(&tx_data[0], &message[0], len * sizeof(uint8_t));
	memcpy(&tx_data[len], &parity[0], 32 * sizeof(uint8_t));
	//noise here
	
	tx_data[2] = 99;
	tx_data[6] = 99;
	tx_data[9] = 99;
	tx_data[13] = 99;
	tx_data[18] = 99;
	tx_data[28] = 99;
	printData(&tx_data[0], len+32);
	
	
	bytes = rs_decode(&tx_data[0], len+32);
	printf("I corrected %d bytes\n", bytes);
	return 0;
}
